# PHOEBE

*** UPDATE OCTOBER 2019: PHOEBE migrated to Git due to the sunsetting of Mercurial by Bitbucket. As a result, you may need to re-clone PHOEBE on your working directory to be up-do-date. ***

PHOEBE (Placing Headgear Optodes Efficiently Before Experiment) is a MATLAB GUI application that measures and displays the optical coupling between fNIRS optodes and the scalp of a subject in real time.

The primary goal of PHOEBE is to help the experimenter optimize signal quality of fNIRS headgear in a time efficient manner. Details on the methods used to quantify the scalp-optode coupling is described in the paper linked at the bottom of this page.

For instructions, please visit the WIKI page.

PHOEBE is distributed under the Creative Commons BY-NC-SA 4.0 License.# PHOEBE

